﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CameraController : MonoBehaviour {

	public GameObject player;

	private Vector3 offset;
	Vector2 touchDeltaPosition;

	void Start ()
	{
		//offset = transform.position - player.transform.position;
	}

	void LateUpdate ()
	{
		//transform.position = player.transform.position + offset;
	}
	// Update is called once per frame
	void Update () {
		if (Input.touchCount == 1)
		{
			Touch touchZero = Input.GetTouch(0);
			if (touchZero.phase == TouchPhase.Moved)
			{
				touchDeltaPosition = Input.GetTouch(0).deltaPosition;
				gameObject.transform.Rotate(touchDeltaPosition.y * .05f, -touchDeltaPosition.x * .4f, 0);
			}
		}else if (Input.GetMouseButton(0))
		{
			float pointer_x = Input.GetAxis("Mouse X");
			float pointer_y = Input.GetAxis("Mouse Y");
			transform.Translate(-pointer_x * 0.5f,
				-pointer_y * 0.5f, 0);
		}else if (Input.GetMouseButton(1))
		{
			float pointer_x = Input.GetAxis("Mouse X");
			float pointer_y = Input.GetAxis("Mouse Y");
			transform.Rotate(-pointer_x * 0.5f,
				-pointer_y * 0.5f, 0);
		}
	}
}
