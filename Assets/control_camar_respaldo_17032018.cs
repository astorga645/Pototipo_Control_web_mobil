using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class control_camar_respaldo_17032018 : MonoBehaviour {
    private Vector2 touchDeltaPosition;	//almacena la posicion del toque 1 en cada Fixedupdate,siempre y cuando se este tocando la pantalla
	private Vector3 mousePosition; //almacena la posiion del cursor siempre y cuando se este precionando el clic izquierdo.
	private float pointer_x;	//alamacena la coordenada en x del cursor
	private float pointer_y;	//alamcena la coordenada en y del cursor
	private float tiempoEspera; //esta variable es utilizada para controlar el tiempo de inacividad que tendra el cursor o el touch para caminar cuando esten activos
	private float tiempo = 0.5f;//esta variable es para ir ajustando el tiempo de inactividad qe debe tener el cursor o touch para caminar
    private float velocidadGiro = 2f;	//esta representa velocidad de giro controlada desde el editable de unity en tiempo de ejecucion
	private float velocidadCaminar = 0.2f; // esta variable tiene el mismo objetivo  funconalidad que la variable anterior solo que para caminar
	public Text deltaToch=null;	// este objeto es para mostrar el valor de la coordenada del toque
	public Text coordeadasDelaTouh=null;// este objeto es solo para mostrar por separado las coordenadas del touch
	public Text contadorTiempo=null;// este objeto es solo para mostrar el valor del tiempo
    public GameObject camara;//este objeto es para manipular las coordenadas de la camara
	public InputField inputVelocidadGiro;//este objeto es para que el player en tiempo de ejecucion manipule la velocidad de giro
	public InputField inputVelocidadCaminar;//este objeto es para que en tiempo de ejecucion el player manipule la velocidad de caminar
	public InputField inputTiempoMovimiento;//este objeto es para que el player indique el tiempo de inactividad par caminar
	public NewBehaviourScript configurando;//este objeto es para saber si el player esta reconfigurando y se desabilita el movimiento


    void Start()
    {
		mousePosition = Input.mousePosition;
		tiempo = float.Parse(inputTiempoMovimiento.text);
		velocidadGiro = float.Parse(inputVelocidadGiro.text);
		velocidadCaminar = float.Parse(inputVelocidadCaminar.text);
		tiempoEspera = tiempo;
    }
	void FixedUpdate(){
	//esta parte esta provicional
		tiempoEspera = float.Parse(inputTiempoMovimiento.text);
		velocidadGiro = float.Parse(inputVelocidadGiro.text);
		velocidadCaminar = float.Parse(inputVelocidadCaminar.text);
	//--------------
		//Se identifica si se esta realizando un gesto touch y no se esta configurando
		coordeadasDelaTouh.text ="coordenada: "+camara.transform.eulerAngles.x;
		if (!configurando.isConfigurando ()) {
			if (Input.touchCount >= 1) {
				contadorTiempo.text = "Tocando: " + tiempo;
				Touch touchZero = Input.GetTouch (0);
				//validacion para determinar si se camina
				if (Vector2.Distance (touchDeltaPosition, Input.GetTouch (0).deltaPosition) == 0f) {
					tiempo -= Time.deltaTime;
				} else {
					tiempo = tiempoEspera;
				}
				//termina validacion
				if (tiempo <= 0f) {
					this.gameObject.transform.Translate (0f, 0f, velocidadCaminar);
				} else {
					touchDeltaPosition = Input.GetTouch (0).deltaPosition;

					if (((camara.transform.eulerAngles.x -(touchDeltaPosition.y*velocidadGiro)) > 300f) || (camara.transform.eulerAngles.x -(touchDeltaPosition.y*velocidadGiro)) < 60f) {
						camara.transform.Rotate (-touchDeltaPosition.y * velocidadGiro, 0f, 0f);
					}
					this.gameObject.transform.Rotate (0f, touchDeltaPosition.x * velocidadGiro, 0f);
					coordeadasDelaTouh.text ="grados x:  "+camara.transform.eulerAngles.x;
				}
			} else if (Input.GetMouseButton(0)) {
				//validacion para determinar si se camina
				contadorTiempo.text = "";
				if (Vector3.Distance (mousePosition, Input.mousePosition) == 0f) {
					tiempo = tiempo - 0.02f;
					contadorTiempo.text = "Cick s: " + tiempo+" delta "+Time.deltaTime;
				} else {
					tiempo = tiempoEspera;
					contadorTiempo.text = "Cick n: " + tiempo+" delta "+Time.deltaTime;
				}
				//termina validacion
				if (tiempo <= 0f) {
					this.gameObject.transform.Translate (0f, 0f, velocidadCaminar);
				} else {
					mousePosition = Input.mousePosition;
					float pointer_y = Input.GetAxis ("Mouse Y");
					float pointer_x = Input.GetAxis ("Mouse X");
					if (((camara.transform.eulerAngles.x -(pointer_y*velocidadGiro)) > 300f && camara.transform.eulerAngles.x <= 360f) || (camara.transform.eulerAngles.x -(pointer_y*velocidadGiro)) < 60f){
						camara.transform.Rotate (-pointer_y * velocidadGiro, 0f, 0f);
					}
					this.gameObject.transform.Rotate (0f, pointer_x * velocidadGiro, 0f);
					//coordeadasDelaTouh.text ="valor del gesto clic: "+pointer_y;
					coordeadasDelaTouh.text ="coordenada: "+camara.transform.eulerAngles.x;
				}

			} else {
				contadorTiempo.text="Esperando ";
			}
		}

	}
    // Update is called once per frame
    void Update()
	{
		deltaToch.text = "dist: "+Vector3.Distance (mousePosition, Input.mousePosition);
		//coordeadasDelaTouh.text ="rotacion x: "+camara.transform.eulerAngles.x; //"Coor: "+ Vector2.Distance (touchDeltaPosition, Input.GetTouch (0).deltaPosition);
	}
	public void asignar_velGiro(float velocidadGiro){
		this.velocidadGiro = velocidadGiro;
	}
	public void asignar_velCaminar(float velocidadCaminar){
		this.velocidadCaminar = velocidadCaminar;
	}
	public void asignar_tiempoEspera(float tiempoEspera){
		this.tiempoEspera = tiempoEspera;
	}
}
