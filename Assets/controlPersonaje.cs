﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
[RequireComponent(typeof(CharacterController))]
public class controlPersonaje : MonoBehaviour {
	private CharacterController control;
	public float velocidad = 2.0f;
	public float velocidadRotacion=3.0f;
	public float tiempo=0.35f;
	public GameObject camara;
	private InputField inputTiempoMovimiento;
	private InputField inputVelocidadGiro;
	private InputField inputVelocidadCaminar;
	public Text contadorTiempo=null;
	private Vector2 touchDeltaPosition;	//almacena la posicion del toque 1 en cada Fixedupdate,siempre y cuando se este tocando la pantalla
	private Vector3 mousePosition; //almacena la posiion del cursor siempre y cuando se este precionando el clic izquierdo.
	// Use this for initialization
	void Start () {
		if (GetComponent<CharacterController> ())
			control = GetComponent<CharacterController> ();
		else
			Debug.LogError("Te hace falta el componente CharacterController");
	}
	
	// Update is called once per frame
	void Update () {
		asignarVariables();
		mirar ();
		caminar( );
	}
	public void mirar(){

		#if UNITY_EDITOR || UNITY_WEBGL || UNITY_STANDALONE
		if (Input.GetMouseButton(0)) {
			if (tiempo > 0f) {
				mousePosition = Input.mousePosition;
				float pointer_y = Input.GetAxis ("Mouse Y");
				float pointer_x = Input.GetAxis ("Mouse X");
				if (((camara.transform.eulerAngles.x +(pointer_y*velocidadRotacion)) > 300f && camara.transform.eulerAngles.x <= 360f) || (camara.transform.eulerAngles.x +(pointer_y*velocidadRotacion)) < 60f){
					camara.transform.Rotate (pointer_y * velocidadRotacion, 0f, 0f);
				}
				this.gameObject.transform.Rotate (0f, -pointer_x * velocidadRotacion, 0f);
			}

		} else {
			contadorTiempo.text="Esperando ";
		}
		#else
		if (Input.touchCount >= 1) {
		contadorTiempo.text = "Tocando: " + tiempo;
		if (tiempo > 0f) {
		touchDeltaPosition = Input.GetTouch (0).deltaPosition;

		if (((camara.transform.eulerAngles.x +(touchDeltaPosition.y*velocidadRotacion)) > 300f) || (camara.transform.eulerAngles.x +(touchDeltaPosition.y*velocidadRotacion)) < 60f) {
		camara.transform.Rotate (touchDeltaPosition.y * velocidadRotacion, 0f, 0f);
		}
		this.gameObject.transform.Rotate (0f, -touchDeltaPosition.x * velocidadRotacion, 0f);
		//coordeadasDelaTouh.text ="grados x:  "+camara.transform.eulerAngles.x;
		}
		}

		#endif
	}
	private void asignarVariables(){
		//esta parte esta provicional
		//tiempo = float.Parse(GameObject.FindGameObjectWithTag("tiempo").GetComponent<InputField>().text);
		velocidadRotacion = float.Parse(GameObject.FindGameObjectWithTag("velGiro").GetComponent<InputField>().text);
		//velocidad = float.Parse(GameObject.FindGameObjectWithTag("velCaminar").GetComponent<InputField>().text);



	}
	/**
	public class example

public float speed=3.0f;
public float rotateSpeed=3.0f;
void update(){
	ChaacterControler controller=GetComponent<CharacterController>();
	transform.Rotate(0,input.GetAxis("Horizontal")*rotateSpeed,0);
	Vector3 forward = transform.TransformDirection(vector3.forward);
	float curSpeed = speed*input.GetAxis("Vertical");
	controller.SimpleMove(forward*curSpeed);
}

	**/
	public void caminar( ){

		#if UNITY_EDITOR || UNITY_WEBGL || UNITY_STANDALONE
		if (Input.GetMouseButton(0)) {
			//validacion para determinar si se camina
			contadorTiempo.text = "";
			if (Vector3.Distance (mousePosition, Input.mousePosition) == 0f) {
				tiempo = tiempo - 0.02f;
				contadorTiempo.text = "Cick s: " + tiempo+" delta "+Time.deltaTime;
			} else {
				tiempo = float.Parse(GameObject.FindGameObjectWithTag("tiempo").GetComponent<InputField>().text);
				contadorTiempo.text = "Cick n: " + tiempo+" delta "+Time.deltaTime;
			}
			if (tiempo <= 0f) {
				//this.gameObject.transform.Translate (0f, 0f, velocidad);

				Vector3 forward = transform.TransformDirection(Vector3.forward);
				Debug.Log("Vector3: "+Vector3.forward+" forword: "+forward);
				control.SimpleMove(forward*velocidad);
			}
		}
		#else
		//validacion para determinar si se camina
		if (Input.touchCount >= 1) {
		if (Vector2.Distance (touchDeltaPosition, Input.GetTouch (0).deltaPosition) == 0f) {
		tiempo -= Time.deltaTime;
		} else {
		tiempo = float.Parse(GameObject.FindGameObjectWithTag("tiempo").GetComponent<InputField>().text);
		}
		if (tiempo <= 0f) {
		Vector3 forward = transform.TransformDirection(Vector3.forward);

		control.SimpleMove(forward*velocidad);
		}
		}
		#endif

	}
	public void mirar2(){

		#if UNITY_EDITOR || UNITY_WEBGL || UNITY_STANDALONE
		if (Input.GetMouseButton(0)) {
			if (tiempo > 0f) {
				mousePosition = Input.mousePosition;
				float pointer_y = Input.GetAxis ("Mouse Y");
				float pointer_x = Input.GetAxis ("Mouse X");
				if (((camara.transform.eulerAngles.x +(pointer_y*velocidadRotacion)) > 300f && camara.transform.eulerAngles.x <= 360f) || (camara.transform.eulerAngles.x +(pointer_y*velocidadRotacion)) < 60f){
					camara.transform.Rotate (pointer_y * velocidadRotacion, 0f, 0f);
				}
				this.gameObject.transform.Rotate (0f, -pointer_x * velocidadRotacion, 0f);
			}

		} else {
			contadorTiempo.text="Esperando ";
		}
		#else
		if (Input.touchCount >= 1) {
		contadorTiempo.text = "Tocando: " + tiempo;
		if (tiempo > 0f) {
		touchDeltaPosition = Input.GetTouch (0).deltaPosition;

		if (((camara.transform.eulerAngles.x +(touchDeltaPosition.y*velocidadRotacion)) > 300f) || (camara.transform.eulerAngles.x +(touchDeltaPosition.y*velocidadRotacion)) < 60f) {
		camara.transform.Rotate (touchDeltaPosition.y * velocidadRotacion, 0f, 0f);
		}
		this.gameObject.transform.Rotate (0f, -touchDeltaPosition.x * velocidadRotacion, 0f);
		//coordeadasDelaTouh.text ="grados x:  "+camara.transform.eulerAngles.x;
		}
		}

		#endif
	}
}
