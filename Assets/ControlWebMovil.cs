﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlWebMovil : MonoBehaviour {
	private Vector2 touchDeltaPosition;	//almacena la posicion del toque 1 en cada Fixedupdate,siempre y cuando se este tocando la pantalla
	private Vector3 mousePosition; //almacena la posiion del cursor siempre y cuando se este precionando el clic izquierdo.
	private float pointer_x;	//alamacena la coordenada en x del cursor
	private float pointer_y;	//alamcena la coordenada en y del cursor
	private float tiempoEspera; //esta variable es utilizada para controlar el tiempo de inacividad que tendra el cursor o el touch para caminar cuando esten activos
	private float tiempo = 0.5f;//esta variable es para ir ajustando el tiempo de inactividad qe debe tener el cursor o touch para caminar
	private float velocidadGiro = 2f;	//esta representa velocidad de giro controlada desde el editable de unity en tiempo de ejecucion
	private float velocidadCaminar = 0.2f; // esta variable tiene el mismo objetivo  funconalidad que la variable anterior solo que para caminar

	public GameObject camara;//este objeto es para manipular las coordenadas de la camara
	void Start()
	{
		mousePosition = Input.mousePosition;
		tiempoEspera = tiempo;
	}
	void FixedUpdate(){
		//esta parte esta provicional
		//--------------
		//Se identifica si se esta realizando un gesto touch y no se esta configurando
		if (true) {
			if (Input.touchCount >= 1) {
				Touch touchZero = Input.GetTouch (0);
				//validacion para determinar si se camina
				if (Vector2.Distance (touchDeltaPosition, Input.GetTouch (0).deltaPosition) == 0f) {
					tiempo -= Time.deltaTime;
				} else {
					tiempo = tiempoEspera;
				}
				//termina validacion
				if (tiempo <= 0f) {
					this.gameObject.transform.Translate (0f, 0f, velocidadCaminar);
				} else {
					touchDeltaPosition = Input.GetTouch (0).deltaPosition;
					if (camara.transform.rotation.x <= 0.7f && camara.transform.rotation.x >= -0.7f) {
						camara.transform.Rotate (-touchDeltaPosition.y * velocidadGiro, 0f, 0f);
					}
					this.gameObject.transform.Rotate (0f, touchDeltaPosition.x * velocidadGiro, 0f);
				}
			} else if (Input.GetMouseButton(0)) {
				//validacion para determinar si se camina
				if (Vector3.Distance (mousePosition, Input.mousePosition) == 0f) {
					tiempo = tiempo - 0.02f;
				} else {
					tiempo = tiempoEspera;
				}
				//termina validacion
				if (tiempo <= 0f) {
					this.gameObject.transform.Translate (0f, 0f, velocidadCaminar);
				} else {
					mousePosition = Input.mousePosition;
					float pointer_y = Input.GetAxis ("Mouse Y");
					float pointer_x = Input.GetAxis ("Mouse X");
					if(camara.transform.rotation.x<=0.7f && camara.transform.rotation.x>=-0.7f){
						camara.transform.Rotate (-pointer_y * velocidadGiro, 0f, 0f);
					}
					this.gameObject.transform.Rotate (0f, pointer_x * velocidadGiro, 0f);
				}

			} 
		}

	}
}
