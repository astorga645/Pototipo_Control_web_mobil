﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NewBehaviourScript : MonoBehaviour {
	private GameObject canvast=null;
	private bool configurando = false;

	public void activarConfiguracionControl(){
		GameObject.FindWithTag("configuracionControl").transform.localPosition  = new Vector3(12,30,0);
		GameObject.FindWithTag("botonEditorConf").transform.localPosition  = new Vector3(-345,126,0);
		configurando = true;
	}
	public void desactivarConfiguracionControl(){
		GameObject.FindWithTag ("botonEditorConf").transform.localPosition  = new Vector3(-148,126,0);
		GameObject.FindWithTag("configuracionControl").transform.localPosition  = new Vector3(-212,30,0);
		configurando = false;

	}
	public bool isConfigurando(){
		return configurando;
	}
}
